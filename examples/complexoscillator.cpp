#include "complexoscillator.h"

ComplexOscillator::ComplexOscillator(float sampleRate,float freq)
{
    float pi = acos(-1.);
    float angFreq = 2*pi*freq/sampleRate;   // Frequency in rad/second.

    // Calculate the angle if rotating once per sample at sampleRate is
    // to produce an angular freq of angFreq.
    deltaPhi = std::complex<float>(cos(angFreq),sin(angFreq));

    // Initialize the phase at any convenient value with abs = 1.
    phase = std::complex<float>(1.,0.);
}

float ComplexOscillator::getSample()
{
    phase *= deltaPhi;          // Rotate phase angle.
    return phase.real();
}
